import axios from 'axios';

export const incidentsProvider = axios.create({
  baseURL: 'http://127.0.0.1:8000/api/localisations/in-rectangle',
  timeout: 180000,
});
