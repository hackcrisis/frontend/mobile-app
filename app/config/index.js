import { Platform } from 'react-native';

export const theme = Object.freeze({
  font: {
    bold: 'Roboto-Bold',
    regular: 'Roboto-Regular',
    light: 'Roboto-Light',
    medium: 'Roboto-Medium',
  },
  color: {
    primary: '#FCB913',
  },
});

export default theme;
