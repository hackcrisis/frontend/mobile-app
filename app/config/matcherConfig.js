export const matcherConfig = Object.freeze({
  MINIMUM_AREA: 200,
  MINIMUM_SIDE: 30,
  MINIMUM_DISTANCE: 0.1,
  TIME_THRESHOLD: 10000,
});

export default matcherConfig;
// TODO: ogarnąć export
