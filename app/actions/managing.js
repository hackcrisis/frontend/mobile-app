import { store } from '../store/store';

export function setPhoneNumber(phone) {
  return dispatch => {
    dispatch({
      type: 'PHONE_NUMBER',
      payload: phone,
    });
  };
}

export function savePath(path) {
  const { paths } = store.getState().mainReducer;
  console.log(paths.length);
  paths.length > 0 ? paths.push(path) : (paths[0] = path);
  return dispatch => {
    dispatch({
      type: 'SET_PATHS',
      payload: paths,
    });
  };
}

export default () => {};
