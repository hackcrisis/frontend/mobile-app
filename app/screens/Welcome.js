import React, { useState } from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import LottieView from 'lottie-react-native';
import { connect } from 'react-redux';
import { TextBold, TextLight } from 'components/Texts';
import { setPhoneNumber } from 'actions/managing';
import coronavirus from '../assets/animations/coronavirus.json';

function Welcome(props) {
  const [phone, onChangePhone] = useState();

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center' }}>
      <TouchableWithoutFeedback
        // style={{ paddingHorizontal: 20, flex: 1 }}
        onPress={() => {
          Keyboard.dismiss();
        }}
      >
        <View style={{ paddingHorizontal: 20, flex: 1 }}>
          <View style={{ flex: 1 }}>
            <LottieView source={coronavirus} autoPlay loop />
          </View>
          <View style={{ flex: 1, justifyContent: 'space-between' }}>
            <View>
              <TextLight style={{ textAlign: 'center', fontSize: 15, color: '#3a3a3a' }}>
                Welcome to WMapuj. Please let us know your phone number in case we will need to
                contact you about infections.
              </TextLight>
              <TextInput
                style={styles.inputStyle}
                onChangeText={text => onChangePhone(text)}
                placeholder="Podaj numer telefonu"
                keyboardType="numeric"
                value={phone}
              />
            </View>
            <TouchableOpacity
              style={{
                backgroundColor: '#24cdef',
                color: 'white',
                paddingVertical: 10,
                paddingHorizontal: 20,
                borderRadius: 20,
                alignItems: 'center',
                alignSelf: 'center',
              }}
              onPress={() => {
                props.setPhoneNumber(phone);
                props.setRegistered();
              }}
            >
              <TextBold style={{ color: 'white', fontSize: 20 }}>Enter</TextBold>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </SafeAreaView>
  );
}

const mapStateToProps = state => ({
  state,
});
const mapDispatchToProps = dispatch => ({
  setPhoneNumber: phone => {
    console.log('phone', phone);
    dispatch(setPhoneNumber(phone));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Welcome);

const styles = StyleSheet.create({
  inputStyle: {
    marginTop: 20,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'gray',
    height: 40,
    padding: 10,
    color: '#3a3a3a',
    fontSize: 16,
  },
});
