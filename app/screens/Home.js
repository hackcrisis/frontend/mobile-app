import React, { Component } from 'react';
import { Text, View, ScrollView, RefreshControl, Modal, Switch } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { TextLight, TextBold, TextMedium } from 'components/Texts';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import coronavirus from '../assets/animations/coronavirus.json';
import check from '../assets/animations/check.json';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      modalVisible: false,
      isLocalizationAllowed: true,
    };
    this.onRefresh = this.onRefresh.bind(this);
    this.onChangeSwitch = this.onChangeSwitch.bind(this);
  }

  onRefresh() {
    this.forceUpdate();
  }

  onChangeSwitch() {
    this.setState(prevState => ({
      isLocalizationAllowed: !prevState.isLocalizationAllowed,
    }));
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />
          }
        >
          <Text style={{ color: 'black' }}>{('modaL:', this.state.modalVisible)}</Text>
          <View style={{ paddingHorizontal: 20 }}>
            <View style={{ flex: 1 }}>
              <View style={{ height: 200, marginVertical: 15 }}>
                <LottieView source={coronavirus} autoPlay loop />
              </View>
              <TextLight style={{ color: '#3a3a3a', fontSize: 20, textAlign: 'center' }}>
                {`Check if infected person \nwas in your neighbourhood`}
              </TextLight>
              <TouchableOpacity
                style={{
                  borderRadius: 20,
                  borderWidth: 2,
                  backgroundColor: 'white',
                  borderColor: '#24cdef',
                  alignSelf: 'center',
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  marginVertical: 20,
                }}
                onPress={() => this.setState({ modalVisible: true })}
              >
                <TextBold style={{ color: '#24cdef' }}>{`Check`.toUpperCase()}</TextBold>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <View
          style={{
            padding: 20,
            borderTopWidth: 1,
            borderTopColor: '#d6d6d6',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 7 }}>
            <View style={{ flex: 1 }}>
              <TextMedium style={{ color: '#3a3a3a', fontSize: 14, paddingBottom: 5 }}>
                Check my location from GPS
              </TextMedium>
              <TextLight style={{ color: '#525252', fontSize: 12 }}>
                Allows downloading your location every 10 minutes
              </TextLight>
            </View>

            <Switch
              trackColor={{ true: '#24cdef' }}
              value={this.state.isLocalizationAllowed}
              onChange={this.onChangeSwitch}
              style={{ width: 50 }}
            />
          </View>
          <View style={{ marginTop: 20 }}>
            <TextMedium
              style={{
                color: '#3a3a3a',
                fontSize: 14,
                paddingBottom: 5,
              }}
            >
              Saved routes
            </TextMedium>
            {this.props.savedPaths.map(path => (
              <Text>{`${moment(path.startTimestamp).calendar()} - ${moment(
                path.endTimestamp
              ).calendar()}`}</Text>
            ))}
          </View>
        </View>

        <Modal animationType="fade" transparent visible={this.state.modalVisible}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(58, 58, 58, .8)',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 20,
                padding: 10,
                width: 260,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <View style={{ height: 100, width: 100, marginVertical: 15 }}>
                <LottieView source={check} autoPlay loop={false} speed={0.5} />
              </View>
              <TextLight
                style={{ color: '#3a3a3a', textAlign: 'center', marginBottom: 10, fontSize: 15 }}
              >
                {/* W Twoim otoczeniu nie znaleziono żadnej zarażonej osoby */}
                In your neighbourhood there was no infected person
              </TextLight>
              <TouchableOpacity
                style={{
                  borderRadius: 20,
                  borderWidth: 2,
                  backgroundColor: 'white',
                  borderColor: '#24cdef',
                  alignSelf: 'center',
                  paddingVertical: 10,
                  paddingHorizontal: 20,
                }}
                onPress={() => this.setState({ modalVisible: false })}
              >
                <Text style={{ color: '#24cdef' }}>Great!</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  phoneNumber: state.mainReducer.phoneNumber,
  savedPaths: state.mainReducer.paths,
});
const mapDispatchToProps = dispatch => ({
  dispatch,
});
export default connect(mapStateToProps, mapDispatchToProps)(Home);
