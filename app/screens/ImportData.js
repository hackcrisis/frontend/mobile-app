import React, { Component } from 'react';
import { StyleSheet, ScrollView, View, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { TextBold, TextRegular, TextLight } from 'components/Texts';
import DocumentPicker from 'react-native-document-picker';
import { connect } from 'react-redux';
import { savePath } from 'actions/managing';

import LottieView from 'lottie-react-native';
import dataDashboard from '../assets/animations/dataDashboard.json';

class ImportData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async pickJson() {
    console.log('pickJson triggered');
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      console.log(
        res.uri,
        res.type, // mime type
        res.name,
        res.size
      );
      this.setState({ documentPickerResponse: res });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  goToMySavedRoutes() {
    this.props.savePath({
      lat: 51.10318,
      lon: 17.028166,
      startTimestamp: 1584800000,
      endTimestamp: 1584801000,
    });
    this.props.navigation.navigate('Home');
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView style={{ flex: 1, paddingHorizontal: 20, paddingVertical: 50 }}>
          <TextRegular style={styles.titleText}>Data import</TextRegular>
          <TextLight style={{ color: '#3a3a3a', fontSize: 12, paddingRight: 30 }}>
            If entering your data from a map by drawing paths is too time consuming, you can import
            data gathered about you from Google
            {/* Jeśli nie chcesz wprowadzać swoich lokalizacji ręcznie, możesz zaimportować dane zebrane
            o Tobie przez Google. */}
          </TextLight>
          <View style={{ height: 200 }}>
            <LottieView source={dataDashboard} autoPlay loop />
          </View>
          {!this.state.documentPickerResponse ? (
            <TouchableOpacity
              onPress={() => this.pickJson()}
              style={{
                borderRadius: 20,
                borderWidth: 2,
                backgroundColor: 'white',
                borderColor: '#207be3',
                alignSelf: 'center',
                paddingVertical: 10,
                paddingHorizontal: 15,
              }}
            >
              <TextBold style={{ color: '#207be3', fontSize: 17 }}>Choose file</TextBold>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => this.goToMySavedRoutes()}
              style={{
                borderRadius: 20,
                borderWidth: 2,
                backgroundColor: 'white',
                borderColor: '#207be3',
                alignSelf: 'center',
                paddingVertical: 10,
                paddingHorizontal: 15,
              }}
            >
              <TextBold style={{ color: '#207be3', fontSize: 17 }}>Save to my routes</TextBold>
            </TouchableOpacity>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  state,
});
const mapDispatchToProps = dispatch => ({
  savePath: path => {
    dispatch(savePath(path));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(ImportData);

const styles = StyleSheet.create({
  titleText: {
    color: '#000000',
    fontSize: 22,
    paddingTop: 20,
    paddingBottom: 10,
  },
});
