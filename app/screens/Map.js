/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';

import { View, StyleSheet, TouchableOpacity, Text, Modal, Alert } from 'react-native';
import MapView, { Polyline } from 'react-native-maps';

import Geolocation from 'react-native-geolocation-service';
import { ScrollView } from 'react-native-gesture-handler';
import DatePicker from 'react-native-date-picker';
import { connect } from 'react-redux';
import { savePath } from 'actions/managing';

import { getDangerousIncidents } from '../utils/matcher';

const moment = require('moment');

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      polylines: [{ coordinates: [] }],
      editing: false,
      initialising: true,
      date: new Date(),
      startDate: new Date(),
      endDate: new Date(),
      pickStartDate: false,
      pickEndDate: false,
    };
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      position => {
        console.log(position);
      },
      error => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  onResponderRelease = () => {
    this.resumeDrawing();
  };

  onPanDrag(e) {
    const { editing } = this.state;
    if (editing) {
      this.setState({
        editing: {
          ...editing,
          coordinates: [...editing.coordinates, e.nativeEvent.coordinate],
        },
        initialising: false,
      });
    }
  }

  setDate(date) {
    const { pickStartDate } = this.state;
    if (pickStartDate) {
      const startDate = date;
      this.setState({ startDate, pickStartDate: false });
    } else {
      const endDate = date;
      this.setState({ endDate, pickEndDate: false });
    }
  }

  onStartShouldSetResponder = evt => {
    if (evt.nativeEvent.touches.length === 2) {
      this.finish();
      return false;
    }
    return true;
  };

  finish() {
    const { editing } = this.state;
    this.setState({
      polylines: [editing],
      editing: false,
      initialising: false,
    });
  }

  clearDrawings() {
    this.setState({
      polylines: [{ coordinates: [] }],
      initialising: false,
    });
  }

  resumeDrawing() {
    this.setState(prevState => ({
      editing: prevState.polylines[0] ?? { coordinates: [] },
      initialising: false,
    }));
  }

  endDatePicker() {
    this.setState(prevState => ({
      endDate: null,
      pickEndDate: true,
      date: prevState.endDate ?? new Date(),
    }));
  }

  startDatePicker() {
    this.setState(prevState => ({
      startDate: null,
      pickStartDate: true,
      date: prevState.startDate ?? new Date(),
    }));
  }

  isTwoFinger(evt) {
    this.setState({ touches: evt.nativeEvent.touches.length });
    // return evt.nativeEvent.touches > 1;
    if (evt.nativeEvent.touches > 1) {
      this.finish();
    } else {
      this.resumeDrawing();
    }
    return true;
  }

  transformData(polyline, startDate, endDate) {
    return polyline.coordinates.map(coordinate => {
      return {
        lat: coordinate.latitude,
        lon: coordinate.longitude,
        startTimestamp: moment(startDate).format('X'),
        endTimestamp: moment(endDate).format('X'),
      };
    });
  }

  prepareAndSendData() {
    const { polylines, startDate, endDate } = this.state;
    const transformedData = this.transformData(polylines[0], startDate, endDate);
    getDangerousIncidents(transformedData).then(response => {
      this.setState({ response });
    });
  }
  /**
   * [
      {
        lat: 51.10318,
        lon: 17.028166,
        startTimestamp: 1584800000,
        endTimestamp: 1584801000,
      },
      {
        lat: 51.083426,
        lon: 17.007622,
        startTimestamp: 1584800450,
        endTimestamp: 1584800460,
      },
      {
        lat: 51.050426,
        lon: 17.000622,
        startTimestamp: 1584800200,
        endTimestamp: 1584800250,
      },
      {
        lat: 57.468426,
        lon: 15.617622,
        startTimestamp: 1584800580,
        endTimestamp: 1584800595,
      },
      {
        lat: 34.123426,
        lon: 5.017622,
        startTimestamp: 1584802000,
        endTimestamp: 1584803000,
      },
    ]
   */

  savePath() {
    // store the following data in redux as a single route:
    /**
     * {
     *  path: this.state.polylines[0],
     *  startTimestamp: this.state.startDate,
     *  endTimestamp: this.state.endDate
     * }
     */
    Alert.alert(
      'What do you want to do',
      'Your route is ready to be exported and probed for potential threat of infection',
      [
        {
          text: 'Save for later',
          onPress: () => {
            this.props.savePath({
              path: this.state.polylines[0],
              startTimestamp: this.state.startDate,
              endTimestamp: this.state.endDate,
            });
          },
        },
        {
          text: 'Save and probe',
          onPress: () => {
            this.props.savePath({
              path: this.state.polylines[0],
              startTimestamp: this.state.startDate,
              endTimestamp: this.state.endDate,
            });
            this.prepareAndSendData();
            this.props.navigation.navigate('Home');
          },
        },
        {
          text: 'Cancel',
          onPress: () => {},
        },
      ]
    );
  }

  minimumDate() {
    return new Date(2019, 11, 1, 0, 0, 0, 0);
  }

  renderBottomBarButtons() {
    return (
      <View style={[styles.bottomBarButtons]}>
        <TouchableOpacity onPress={() => this.startDatePicker()} style={{ flex: 1 }}>
          {this.renderTimePlaceholder(
            'startDate',
            'Start date',
            this.state.startDate
              ? `${this.state.startDate.toLocaleDateString()}\n${this.state.startDate.toLocaleTimeString()}`
              : '',
            'rgba(0, 150, 192, 0.8)'
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.endDatePicker()} style={{ flex: 1 }}>
          {this.renderTimePlaceholder(
            'endDate',
            'End date',
            this.state.endDate
              ? `${this.state.endDate.toLocaleDateString()}\n${this.state.endDate.toLocaleTimeString()}`
              : '',
            'rgba(255, 0, 102, 0.8)'
          )}
        </TouchableOpacity>
      </View>
    );
  }

  renderDrawButton() {
    return (
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => (this.state.editing ? this.finish() : this.resumeDrawing())}
          style={[
            styles.bubble,
            styles.button,
            styles.centeredItem,
            this.state.editing ? styles.editsButton : styles.noEditsButton,
          ]}
        >
          <Text>{this.state.editing ? 'Stop' : 'Draw'}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderClearDrawingsButton() {
    return (
      <View style={styles.clearButtonContainer}>
        <TouchableOpacity
          onPress={() => this.clearDrawings()}
          style={[
            styles.bubble,
            // styles.button,
            styles.centeredItem,
            this.state.editing ? styles.editsButton : styles.noEditsButton,
          ]}
        >
          <Text>Clear</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderSaveButton() {
    return (
      <View style={styles.saveButtonContainer}>
        <TouchableOpacity
          onPress={() => this.savePath()}
          style={[
            styles.bubble,
            // styles.button,
            styles.centeredItem,
            this.state.editing ? styles.editsButton : styles.noEditsButton,
          ]}
        >
          <Text>Save</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderTimePlaceholder(key, title, currentDateString, dateColor) {
    return (
      <View style={{ flex: 1, marginHorizontal: 5 }}>
        <View style={{ flex: 2, justifyContent: 'center' }}>
          <Text style={{ fontSize: 14, color: 'black', fontWeight: 'bold' }}>{title}</Text>
        </View>
        <View style={{ flex: 3 }}>
          <Text style={{ fontSize: 17, color: dateColor ?? 'rgba(120,40,120, 1.0)' }}>
            {currentDateString}
          </Text>
        </View>
      </View>
    );
  }

  renderLogs() {
    return (
      <View style={{ minHeight: 40, backgroundColor: 'yellow', maxHeight: 250 }}>
        <ScrollView>
          <Text>{JSON.stringify(this.state.touches)}</Text>
          <Text>{JSON.stringify(this.state.response)}</Text>
          {/* <Text>{JSON.stringify(this.state.startDate)}</Text>
          <Text>{JSON.stringify(this.state.endDate)}</Text> */}
          <Text>{JSON.stringify(this.state.polylines[0].coordinates)}</Text>
        </ScrollView>
      </View>
    );
  }

  renderMap() {
    return (
      <View style={{ flex: 1 }}>
        <MapView
          style={{ flex: 1 }}
          pitchEnabled={false}
          showsIndoors
          showsUserLocation
          showsMyLocationButton
          followsUserLocation={this.state.initialising}
          scrollEnabled={!this.state.editing}
          onPanDrag={e => this.onPanDrag(e)}
        >
          <Polyline
            key="editingPolyline"
            coordinates={this.state.editing.coordinates ?? this.state.polylines[0].coordinates}
            strokeColor={this.state.editing ? 'rgba(255,0,0,0.5)' : 'rgba(255,255,255,0.2)'}
            fillColor={this.state.editing ? 'rgba(255,0,0,0.5)' : 'rgba(255,255,255,0.2)'}
            strokeWidth={3}
          />
        </MapView>
        {this.renderDrawButton()}
        {this.renderClearDrawingsButton()}
        {this.renderSaveButton()}
      </View>
    );
  }

  renderModals() {
    return (
      <Modal
        animationType="slide"
        transparent
        visible={this.state.pickStartDate || this.state.pickEndDate}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignContent: 'center',
            }}
          >
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 18,
                color: 'black',
                marginHorizontal: 10,
                textAlign: 'center',
              }}
            >
              Pick approximate date and time the drawn route has{' '}
              {this.state.pickStartDate ? 'started' : ''}
              {this.state.pickEndDate ? 'finished' : ''}
            </Text>
          </View>
          <View style={{ flex: 4, backgroundColor: 'white' }}>
            <DatePicker
              style={[styles.datePicker]}
              date={this.state.date}
              onDateChange={date => this.setDate(date)}
              minimumDate={this.minimumDate()}
              maximumDate={new Date()}
            />
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        {this.renderMap()}
        {/* {this.renderLogs()} */}
        {/* {this.renderDrawButton()} */}
        {this.renderBottomBarButtons()}
        {this.renderModals()}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state,
});
const mapDispatchToProps = dispatch => ({
  savePath: path => {
    dispatch(savePath(path));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(Map);

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  editsButton: {
    backgroundColor: 'red',
  },
  noEditsButton: {
    backgroundColor: 'gray',
  },
  centeredItem: {
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
  },
  clearButtonContainer: {
    position: 'absolute',
    top: 20,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
  },
  saveButtonContainer: {
    position: 'absolute',
    top: 20,
    left: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20,
  },
  datePicker: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    alignContent: 'center',
    backgroundColor: 'rgba(255,255,255,0.9)',
    marginVertical: 190,
    marginHorizontal: 50,
  },
  bottomBarButtons: {
    // flex: 1,
    minHeight: 90,
    maxHeight: 150,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    // justifyContent: "center"
  },
});
