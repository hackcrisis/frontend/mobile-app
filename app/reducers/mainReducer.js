const initialState = {
  phoneNumber: '',
  paths: [],
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'PHONE_NUMBER': {
      return {
        ...state,
        phoneNumber: action.payload,
      };
    }
    case 'SET_PATHS': {
      return {
        ...state,
        paths: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
export default mainReducer;
