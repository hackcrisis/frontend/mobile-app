/* eslint-disable react/no-unused-prop-types */
import React from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

const HeaderLeft = () => (
  <TouchableOpacity style={{ flex: 1, paddingLeft: 5 }} onPress={() => {}}>
    {/* <Icon name={props.icon} size={props.size ? props.size : 25} /> */}
  </TouchableOpacity>
);

HeaderLeft.propTypes = {
  icon: PropTypes.string,
  size: PropTypes.number,
};

export default HeaderLeft;
