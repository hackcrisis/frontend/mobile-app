import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import theme from 'config';

export const TextBold = props => {
  const { children, style } = props;
  return (
    <Text selectable style={[style, { fontFamily: theme.font.bold }]}>
      {children}
    </Text>
  );
};
TextBold.defaultProps = {
  style: { color: '#3A3A3A' },
};
TextBold.propTypes = {
  children: PropTypes.node.isRequired,
  style: Text.propTypes.style,
};

export const TextRegular = props => {
  const { children, style } = props;
  return (
    <Text selectable style={[style, { fontFamily: theme.font.regular }]}>
      {children}
    </Text>
  );
};
TextRegular.defaultProps = {
  style: { color: '#3A3A3A' },
};
TextRegular.propTypes = {
  children: PropTypes.node,
  style: Text.propTypes.style,
};

export const TextMedium = props => {
  const { children, style } = props;
  return (
    <Text selectable style={[style, { fontFamily: theme.font.medium }]}>
      {children}
    </Text>
  );
};
TextMedium.defaultProps = {
  style: { color: '#3A3A3A' },
};
TextMedium.propTypes = {
  children: PropTypes.node.isRequired,
  style: Text.propTypes.style,
};

export const TextLight = props => {
  const { children, style } = props;
  return (
    <Text selectable style={[style, { fontFamily: theme.font.regular }]}>
      {children}
    </Text>
  );
};
TextLight.defaultProps = {
  style: { color: '#3A3A3A' },
};
TextLight.propTypes = {
  children: PropTypes.node.isRequired,
  style: Text.propTypes.style,
};
