const turf = require('@turf/turf');
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const matcherConfig = require("../config/matcherConfig");


function filterIncidents (localisationHistory, incidents) {
  const filteredByDistance = filterByDistance(localisationHistory, incidents)
  console.log(`[Filtered by distance] ${JSON.stringify(filteredByDistance)}`)
  const filteredByTime = filterByTime(localisationHistory, filteredByDistance)
  console.log(`[Filtered by time] ${JSON.stringify(filteredByTime)}`)
  return filteredByTime
}

function filterByDistance(localisationHistory, incidents) {
  let filteredByDistance = [];
  incidents.forEach(function (incident) {
    const { lat, lon } = incident;
    const incidentPoint = turf.point([lat, lon]);
    localisationHistory.forEach(function (localisationEntry) {
      const historyEntryPoint = turf.point([localisationEntry.lat, localisationEntry.lon]);
      const distance = turf.distance(incidentPoint, historyEntryPoint);
      if (distance <= matcherConfig.MINIMUM_DISTANCE) {
        console.log(`(${incidentPoint.geometry.coordinates}) - (${historyEntryPoint.geometry.coordinates}) = ${distance}`);
        filteredByDistance.push(incident);
      }
    });
  });
  filteredByDistance = new Set(filteredByDistance);
  return [...filteredByDistance];
}

function filterByTime(localisationHistory, filteredByDistance) {
  let filteredByTime = []
  filteredByDistance.forEach(incident => {
    const { startTimestamp, endTimestamp } = incident
    const incidentRange = moment.range(startTimestamp - matcherConfig.TIME_THRESHOLD, endTimestamp + matcherConfig.TIME_THRESHOLD)
    localisationHistory.forEach(historyEntry => {
      const { startTimestamp, endTimestamp } = historyEntry
      const historyRange = moment.range(startTimestamp, endTimestamp)
      if (incidentRange.intersect(historyRange)) {
        filteredByTime.push(historyEntry)
      }
    }
    )
  }
  )
  filteredByTime = new Set(filteredByTime)
  return [...filteredByTime]
}

module.exports = { filterIncidents } 
