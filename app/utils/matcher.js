const areaPreparation = require('./areaPreparation');
const incidentsFiltering = require('./incidentsFiltering');
const communication = require('./communicationModule');

// eslint-disable-next-line import/prefer-default-export
export async function getDangerousIncidents(localisationHistory) {
  const area = areaPreparation.computeArea(localisationHistory);
  let incidents = null;
  try {
    incidents = await communication.fetchIncidentsInArea(area);
    incidents = incidentsFiltering.filterIncidents(localisationHistory, incidents);
    return incidents;
  } catch (error) {
    return `[${new Date().toLocaleString()}] Data fetch from server failed!`;
  }
}
