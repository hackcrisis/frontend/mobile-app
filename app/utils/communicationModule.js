const incidentsProvider = require('../config/network');

async function fetchIncidentsInArea(area) {
  const { x, y, height, width } = area;
  const response = await incidentsProvider.get(URL, {
    params: {
      x,
      y,
      h: height,
      w: width,
    },
  });
  return response.data.map;
}

module.exports = { fetchIncidentsInArea };
