import React, { useState } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { store, persistor } from './store/store';
import Stack from './router';
import Welcome from './screens/Welcome';

function App() {
  const [registered, setRegistered] = useState(false);
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {registered ? <Stack /> : <Welcome setRegistered={() => setRegistered(true)} />}
      </PersistGate>
    </Provider>
  );
}

export default App;
