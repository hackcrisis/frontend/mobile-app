/* eslint-disable react/prop-types */
import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HeaderLeft from 'components/HeaderLeft';
import Home from 'screens/Home';
import Map from 'screens/Map';
import ImportData from 'screens/ImportData';

const HomeStack = createStackNavigator(
  {
    Home: { screen: Home, navigationOptions: () => ({ header: null }) },
  },
  {
    initialRouteName: 'Home',
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderLeft navigation={navigation} icon="chevron-left" />,
      tabBarVisible: !(navigation.state.index > 15),
      tabBarIcon: ({ focused }) =>
        focused ? (
          <Icon name="home" size={25} color="#24cdef" />
        ) : (
          <Icon name="home" size={25} color="#999" />
        ),
    }),
  }
);

const MapStack = createStackNavigator(
  {
    Map: { screen: Map, navigationOptions: () => ({ header: null }) },
  },
  {
    initialRouteName: 'Map',
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderLeft navigation={navigation} icon="chevron-left" />,
      tabBarVisible: !(navigation.state.index > 15),
      tabBarIcon: ({ focused }) =>
        focused ? (
          <Icon name="drafting-compass" size={25} color="#24cdef" />
        ) : (
          <Icon name="drafting-compass" size={25} color="#999" />
        ),
    }),
  }
);

const ImportStack = createStackNavigator(
  {
    ImportData: { screen: ImportData, navigationOptions: () => ({ header: null }) },
  },
  {
    initialRouteName: 'ImportData',
    navigationOptions: ({ navigation }) => ({
      headerLeft: <HeaderLeft navigation={navigation} icon="chevron-left" />,
      tabBarVisible: !(navigation.state.index > 15),
      tabBarIcon: ({ focused }) =>
        focused ? (
          <Icon name="upload" size={25} color="#24cdef" />
        ) : (
          <Icon name="upload" size={25} color="#999" />
        ),
    }),
  }
);

const Stack = createBottomTabNavigator(
  {
    Map: {
      screen: MapStack,
    },
    Home: {
      screen: HomeStack,
    },
    ImportData: {
      screen: ImportStack,
    },
  },
  {
    tabBarOptions: {
      style: {
        height: 60,
      },
      showLabel: false,
    },
    initialRouteName: 'Home',
    animationEnabled: true,
    lazy: true,
  }
);

export default createAppContainer(Stack);
