import { AsyncStorage } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'; // Imports: Redux
import thunk from 'redux-thunk';

import rootReducer from '../reducers/index';
import Reactotron from '../ReactotronConfig';

// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  blacklist: ['mainReducer'],
  // Blacklist (Don't Save Specific Reducers)
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(
  persistedReducer,
  compose(applyMiddleware(thunk), Reactotron.createEnhancer())
);
const persistor = persistStore(store);
export { store, persistor };
